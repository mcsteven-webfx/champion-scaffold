<header class="masthead">
    <div class="container-fluid">
        <div class="masthead-img">
                <?php echo fx_get_image_tag( 206 ); ?>
        </div>
            
        <div class="masthead-text">
            <div class="masthead-pattern">
                <?php echo fx_get_image_tag( 235 ); ?>
            </div>
                 <div class="container">
                    <?php if ( is_search() ): ?>
                        <h3 class="h1">Search Results</h3><?php /* different heading type for SEO benefit */ ?>
                    <?php elseif ( is_home() ): ?>
                        <h3 class="h1">Blog</h3><?php /* different heading type for SEO benefit */ ?>
                    <?php elseif ( is_404() ) : ?>
                        <h1>404: Oops! We can't find the page you're looking for.</h1>
                    <?php else : ?>
                        <h1><?php the_title(); ?></h1>
                    <?php endif; ?>
                     <div class="breadcrumbs">
                        <?php
                            if( function_exists( 'yoast_breadcrumb' ) ) {
                                yoast_breadcrumb( '<div class="breadcrumbs hidden-sm-down">', '</div>' );
                            }
                        ?>
                    </div>
                </div>
        </div>
    </div>
</header>
