var FX = ( function( FX, $ ) {


	$( () => {
		FX.HomepageMasthead.init()
	})


	FX.HomepageMasthead = {
		$slider: null,

		init() { 
			
			this.$slider = $('.homepage-slider')

			if( this.$slider.length ) {
				this.applySlick()
			}
		},

		applySlick() {
            this.$slider.slick( {
                dots: false,
                autoplay: true,
                autoplaySpeed: 5000,
            });
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )