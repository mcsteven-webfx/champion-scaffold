var FX = ( function( FX, $ ) {


	$( () => {
		FX.HomeGallery.init()
	})


	FX.HomeGallery = {
		$slider: null,

		init() {
			this.$slider = $('.js-gallery-slider')

			if( this.$slider.length ) {
				this.applySlick()
			}
		},

		applySlick() {
            this.$slider.slick( {
                dots: false,
				infinite: true,
				arrows: true,
				speed: 300,
				slidesToShow: 3,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 1199,
						settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						arrows: true,
						}
					},
					{
						breakpoint: 600,
						settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						arrows: true,
						}
					},
					{
						breakpoint: 480,
						settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: true,
						}
					}
				]
            });
		}
	}

	

	return FX

} ( FX || {}, jQuery ) )

