<?php
/**
 * Register FX blocks
 * 
 * fx_register_block() is, at its core, a wrapper function for acf_register_block_type with additional parameters for 
 * our supporting functionality 
 * 
 * @see Guru card: https://app.getguru.com/card/Tn9zzk8c/FX-ACF-Blocks
 * @see more info for acf_register_block_type(): https://www.advancedcustomfields.com/resources/acf_register_block_type/
 * 
 * Below is a reference for the parameters you can pass to fx_register_block(). You can also pass any setting from 
 * acf_register_block_type() to fx_register_block().
 * 
 * Required arguments: "name", "title", and "template"
 * 
 */
$reference_settings = [
    // required
    'name'                  => '', // (string) unique name to identify block (no spaces)
    'title'                 => '', // (string) display title for block
    'template'              => '', // (string) relative path of the template for block (e.g "/block-templates/innerpage/template.php")
    
    // optional
    'css'                   => '', // (string) block-specific stylesheet. Assumed to be in /themes/fx/assets/css, so use relative path (e.g. "homepage/homepage-block.css")
    'css_deps'              => [], // (array|string) CSS dependency handles. These will be loaded before block's stylesheet. Dependencies must already be registered
    'js'                    => '', // (string) block-specific script. Assumed to be in /themes/fx/assets/js, so use relative path (e.g. "homepage/homepage-block.js")
    'js_deps'               => [], // (array|string) JS dependency handles. These will be loaded before block's script. Dependencies must already be registered
    'description'           => '', // (string) short, useful description of block to indicate block's purpose
    'category'              => '', // (string) category for where block appears in Block Library
    'icon'                  => '', // (array|string) can be a dashicon or SVG image used to identify the block
    'keywords'              => '', // (array) terms to help find block in block editor
    'post_types'            => [], // (array) if declared, will restrict block to being available for only specified post types. Default is "page"
    'exclude_post_types'    => [], // (array) post types that block should NOT appear for
    'mode'                  => '', // (string) display mode for block when you add block in Block Editor
    'supports'              => '', // (associative array) features to support. See https://developer.wordpress.org/block-editor/developers/block-api/block-supports/
];
/**
 * General blocks
 * 
 * These blocks are intended to be used anywhere, including the homepage and innerpage.
 * 
 * Block template path: /themes/fx/block-templates/general
 * Stylesheet path:     /themes/fx/assets/css/general
 * Script path:         /themes/fx/assets/js/general
 * 
 */
/**
 * Create a "FX General Blocks" category in the block editor. Use "fx-general-blocks" as your "category" value in 
 * fx_register_block()
 * 
 */
fx_add_block_category( 'FX General Blocks', 'fx-general-blocks' );
/**
 * Plan WYSIWYG block for general usage
 * 
 */
fx_register_block(
    [
        'name'          => 'wysiwyg',
        'title'         => 'WYSIWYG',
        'template'      => 'general/wysiwyg.php',
        'description'   => 'A basic "What you see is what you get" editor.',
        'css'           => 'general/wysiwyg.css',
        'post_types'    => [],
    ]
);
fx_register_block(
    [
        'name'          => 'CTA General',
        'title'         => 'General - CTA Block',
        'template'      => 'general/cta-bar.php',
        'description'   => 'CTA Block',
        'css'           => 'general/cta-bar.css',
        'post_types'    => [],
    ]
);
fx_register_block(
    [
        'name'          => 'Image-button General',
        'title'         => 'General - Image Button Block',
        'template'      => 'homepage/image-button.php',
        'description'   => 'General image Button Block',
        'css'           => 'general/icon-cards.css',
        'post_types'    => [],
    ]
);
/*
fx_register_block(
    [
        'name'          => 'General fxa-accordion',
        'title'         => 'General - Fxa Accordion Block',
        'template'      => 'general/fxa-accordion.php',
        'description'   => 'Fxa Accordion Block',
        'css'           => 'css/components/FxAccordion.css',
        'js'            => 'components/FxAccordion.js',
        'post_types'    => [],
    ]
);*/
/*
fx_register_block(
    [
        'name'          => 'Gallery General',
        'title'         => 'General - Gallery Block',
        'template'      => 'general/gallery.php',
        'description'   => 'Gallery Block',
        'css'           => 'homepage/case-study-gallery.css',
        'css_deps'      => [ 'fx_slick' ],
        'js_deps'       => [ 'fx_slick' ],
        'post_types'    => [],
    ]
);*/
/**
 * To avoid issues with CF7 assets, we're creating our own CF7 block. You shouldn't need to touch this section.
 *
 */
$cf7_settings = [
    'name'          => 'cf7-block',
    'title'         => 'CF7 Block',
    'template'      => 'general/cf7-block.php',
    'description'   => 'Adds a CF7 block to page',
    'css_deps'      => [ 'fx_choices_custom', 'contact-form-7' ],
    'js_deps'       => [ 'contact-form-7', 'wpcf7-recaptcha', 'google-recaptcha' ],
    'keywords'      => [ 'cf7', 'contact', 'form' ],
    'mode'          => 'edit',
    'post_types'    => [], // all post types
];
$cf7_icon = WP_PLUGIN_DIR . '/contact-form-7/assets/icon.svg';
if( file_exists( $cf7_icon ) ) {
    $cf7_settings['icon'] = file_get_contents( $cf7_icon );
}
fx_register_block( $cf7_settings );
/**
 * Homepage blocks
 * 
 * These blocks are intended to be used ONLY on the homepage.
 * 
 * Block template path: /themes/fx/block-templates/homepage
 * Stylesheet path:     /themes/fx/assets/css/homepage
 * Script path:         /themes/fx/assets/js/homepage
 * 
 */
/**
 * Create a "FX Homepage Blocks" category in the block editor. Use "fx-homepage-blocks" as your "category" value in 
 * fx_register_block()
 * 
 */
fx_add_block_category( 'FX Homepage Blocks', 'fx-homepage-blocks' );
/**
 * This is the main homepage "outer block." All other homepage blocks should be added within this block in the Block 
 * Editor and in block-templates/homepage/homepage-block.php
 * 
 */
fx_register_block(
    [
        'name'          => 'homepage-block',
        'title'         => 'Homepage',
        'template'      => 'homepage/homepage-block.php',
        'description'   => 'The main content block for the homepage',
        'mode'          => 'preview',
        'supports'      => [ 'jsx' => true ], // enables support for inner blocks
        'category'      => 'fx-homepage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'homepage-masthead-slider',
        'title'         => 'Homepage - Masthead Slider',
        'template'      => 'homepage/masthead-slider.php',
        'description'   => 'Slider block for the homepage masthead.',
        'css'           => 'homepage/masthead.css',
        'js'            => 'homepage/masthead-slider.js',
        'css_deps'      => [ 'fx_slick' ],
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-homepage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'image-button',
        'title'         => 'Homepage - Image Button Block',
        'template'      => 'homepage/image-button.php',
        'description'   => 'image Button Block',
        'css'           => 'homepage/icon-cards.css',
        'category'      => 'fx-homepage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'left-image-right-text',
        'title'         => 'Homepage - Left Image Right Text Block',
        'template'      => 'homepage/left-image-right-text.php',
        'description'   => 'Left Image Right Text Block',
        'css'           => 'homepage/service.css',
        'category'      => 'fx-homepage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'case-study-gallery',
        'title'         => 'Homepage - Case Study Gallery Block',
        'template'      => 'homepage/case-study-gallery.php',
        'description'   => 'Case Study Gallery Block',
        'css'           => 'homepage/case-study-gallery.css',
        'css_deps'      => [ 'fx_slick' ],
        'js'            => 'homepage/gallery-slider.js',
        'js_deps'       => [ 'fx_slick' ],
        'category'      => 'fx-homepage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'image-text',
        'title'         => 'Homepage - Image Text Block',
        'template'      => 'homepage/image-text.php',
        'description'   => 'image Text Block',
        'css'           => 'homepage/contained-image-text.css',
        'category'      => 'fx-homepage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'cta',
        'title'         => 'Homepage - CTA Block',
        'template'      => 'homepage/cta-bar.php',
        'description'   => 'Homepage CTA Block',
        'css'           => 'homepage/cta-bar.css',
        'category'      => 'fx-homepage-blocks',
    ]
);
/**
 * Innerpage blocks
 * 
 * These blocks are intended to be used ONLY on innerpages
 * 
 * Block template path: /themes/fx/block-templates/innerpage
 * Stylesheet path:     /themes/fx/assets/css/innerpage
 * Script path:         /themes/fx/assets/js/innerpage
 * 
 */
fx_register_block(
    [
        'name'          => 'half-and-half',
        'title'         => 'Innerpage - Half and Half Block',
        'template'      => 'innerpage/half-and-half.php',
        'description'   => 'Innerpages Half and Half Block',
        'css'           => 'innerpage/half-image-text.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'full-width-image-text',
        'title'         => 'Innerpage - Full Width Image Text Block',
        'template'      => 'innerpage/full-width-image-text-box-overlay.php',
        'description'   => 'Innerpages Full Width Image Block',
        'css'           => 'innerpage/full-width-image-text.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'Rental',
        'title'         => 'Innerpage - Rental Block',
        'template'      => 'innerpage/rental.php',
        'description'   => 'Innerpages Rental Block',
        'css'           => 'innerpage/rental-cart.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'Rental Quote',
        'title'         => 'Innerpage - Rental Quote Block',
        'template'      => 'innerpage/rental-quote.php',
        'description'   => 'Innerpages Rental Block',
        'css'           => 'innerpage/rental-quote.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);
fx_register_block(
    [
        'name'          => 'contact-block',
        'title'         => 'Contact Block',
        'template'      => 'innerpage/contact-block.php',
        'description'   => 'Form on contact page with intro content and team members (up to 5)',
        'css'           => 'innerpage/contact-section.css',
        'category'      => 'fx-innerpage-blocks',
        'css_deps'      => [ 'fx_choices_custom', 'contact-form-7' ],
        'js_deps'       => [ 'contact-form-7', 'wpcf7-recaptcha', 'google-recaptcha' ],
    ]
);
fx_register_block(
    [
        'name'          => 'accordion-block',
        'title'         => 'Accordion Block',
        'template'      => 'innerpage/accordion.php',
        'css'           => 'innerpage/accordion.css',
        'category'      => 'fx-innerpage-blocks',
        'js_deps'       => [ 'fx_accordion' ],
    ]
);
fx_register_block(
    [
        'name'          => 'fxa-accordion',
        'title'         => 'Innerpage - Fxa Accordion Block',
        'template'      => 'innerpage/fxa-accordion.php',
        'description'   => 'Innerpages Fxa Accordion Block',
        'css'           => '../components/FxAccordion.css',
        'js'            => '../components/FxAccordion.js',
        'category'      => 'fx-innerpage-blocks',
    ]
);
/**
 * Create a "FX Innerpage Blocks" category in the block editor. Use "fx-innerpage-blocks" as your "category" value in 
 * fx_register_block()
 * 
 */
fx_add_block_category( 'FX Innerpage Blocks', 'fx-innerpage-blocks' );
