<section class="image-text image-text--left section-margins fx-starter-block__section">
    <div class="container">
        <div class="row flex-row">
            
            <div class="col-md-6 col-md-offset-1 col-md-push-5 image-text__half image-text__text">
                <h5><?php echo get_field('heading'); ?></h5>
                <h2><?php echo get_field('title'); ?></h2>
                <?php echo get_field('paragraph'); ?>
            </div>
            <div class="col-md-5 col-md-pull-7 image-text__half image-text__img"> 
                 <?php  echo fx_get_image_tag( get_field( 'image' ), false, true ); ?>  
            </div>
        </div>
    </div>
</section>