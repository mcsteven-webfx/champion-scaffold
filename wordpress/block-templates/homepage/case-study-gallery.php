<section class="case-study-gallery">
        <div class="container-fluid">
            <div class="case-study-content">
                <div class="case-study-pattern">
                    <img src="../wp-content/themes/championscaffold/assets/img/case-bg.png" class="img-responsive" alt="">
                </div>
                <div class="row">
                    <div class="col-lg-6 col-lg-push-6">
                        <div class="case-study-text clearfix">
                            <div class="case-left">
                                <h5><?php echo get_field( 'title' ); ?></h5>
                                <h3><?php echo get_field( 'heading' ); ?></h3>
                            </div>
                            <div class="case-btn">
                                
                                <?php if( $button = get_field( 'button' )  ): ?>
                                <a href="<?php echo esc_url( $button['url'] ); ?>" <?php echo !empty($button['target']) ? 'target="' . esc_url( $button['target'] ) . '"': ''; ?> class="btn btn-primary">
                                    <?php echo $button['title']; ?>
                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-pull-6">
                        <div class="gallery-section">
                        	
                            <div class="js-gallery-slider fx-slider">
                                 <?php while(have_rows('gallery')): the_row();
                                 	 $logo = get_sub_field( 'image_logo' );
                    				 $url  = get_sub_field( 'image_url' );
                                 ?>
                                <div class="gallery-item fx-slide">
                                    <a href="<?php if( $url ) { echo $url; } ?>" target="_blank">
                                    	<div class="gallery-pict">
	                                        <div class="img-responsive">
	                                              <?php if( $logo ) {
						                                echo fx_get_image_tag( $logo );
						                           } ?>
	                                        </div>
	                                        <div class="gallery-text">
	                                            <?php echo get_sub_field( 'title' ); ?>
	                                        </div>
                                    	</div>
                                     </a>
                                     
                                </div>
                                 <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    