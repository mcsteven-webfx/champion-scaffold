
    <section class="banner ">
        <div class="container-fluid">
            <div class="hero-container">
                <div class="hero-img">
                    <?php echo fx_get_image_tag( get_field( 'background_image' ), false, true ); ?>
                </div>
                <div class="hero-content">
                    <div class="hero-design">
                        <img src="../wp-content/themes/championscaffold/assets/img/hero-design.png" alt="">
                    </div>
                    <div class="hero-text">
                        <h1><?php the_field( 'title' ); ?></h1>
                        <?php the_field( 'headline' ); ?>
                         <?php if( $button = get_field( 'button' )  ): ?>
                            <a
                                class="btn btn-primary"
                                href="<?php echo esc_url( $button['url'] ); ?>"
                            >
                                <?php echo $button['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
