<div class="container-fluid">
        <section class="cards fx-starter-block__section">
            <div class="cards-bg">
                <?php echo fx_get_image_tag( get_field( 'background_image' ), false, true ); ?>
            </div>
            <div class="container">
                <div class="row card-flex">
                    <div class="col-md-3 col-xs-12 col-xxs-12">
                        <div class="card-main-text">
                            <h5><?php echo get_field( 'title' ); ?></h5>
                            <h3><?php echo get_field( 'paragraph' ); ?></h3>
                        </div>
                    </div>
                    <?php while(have_rows('icon')): the_row(); ?>
                    <div class="col-md-3 col-xs-4 col-xxs-12 soft-bottom card-item">
                        <div class="card card--static">
                            
                                <div class="card__top">
                                    <div class="card__img-wrap">
                                        <div class="card__icon">
                                            <div class="gray-box">
                                                <img src="../wp-content/themes/championscaffold/assets/img/gray-box.png" alt="">
                                               
                                            </div>
                                            <i class="icon"><?php echo fx_get_image_tag( get_sub_field( 'image' ), false, true ); ?></i>
                                            
                                        </div>
                                    </div>
                                    <div class="card__details">
                                        <h4 class="card__title"><?php echo get_sub_field( 'title' ); ?></h4>
                                        <div class="card__description">
                                            <?php echo get_sub_field( 'paragraph' ); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card__bottom">
                                    <?php if( $button = get_sub_field( 'button' )  ): ?>
                                        <a href="<?php echo esc_url( $button['url'] ); ?>" <?php echo !empty($button['target']) ? 'target="' . esc_url( $button['target'] ) . '"': ''; ?> class="btn btn-tertiary">
                                            <?php echo $button['title']; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
    </div>