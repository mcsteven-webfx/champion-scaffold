<div class="container-fluid">
    <section class="cta-bar fx-starter-block__section img-responsive">
        <div class="cta-bar__bg">
             <?php echo fx_get_image_tag( get_field( 'image' ), false, true ); ?>  
        </div>
        <div class="cta-bar__content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h5><?php echo get_field( 'title' ); ?></h5>
                        <h3><?php echo get_field( 'heading' ); ?></h3>
                        <?php if( $button = get_field( 'button' )  ): ?>
                                <a href="<?php echo esc_url( $button['url'] ); ?>" <?php echo !empty($button['target']) ? 'target="' . esc_url( $button['target'] ) . '"': ''; ?> class="btn btn-primary">
                                    <?php echo $button['title']; ?>
                                </a>
                        <?php endif; ?>
                        <p><?php echo get_field( 'call' ); ?> <a href=""><?php echo get_field( 'phone' ); ?></a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>