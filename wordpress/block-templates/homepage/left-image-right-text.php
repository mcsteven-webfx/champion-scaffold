<div class="container-fluid">
        <section class="service-section">
            <div class="service-img">
                 <?php echo fx_get_image_tag( get_field( 'image' ), false, true ); ?> 
            </div>
            <div class="service-content clearfix">
                <div class="service-pattern">
                    <img src="../wp-content/themes/championscaffold/assets/img/services-pattern.png" class="img-responsive" alt="">
                </div>
                <div class="services-wrapper">
                     <h5><?php echo get_field( 'title' ); ?></h5>
                     <h2><?php echo get_field( 'heading' ); ?></h2>
                     <?php echo get_field( 'paragraph' ); ?>
                    <ul class="service-tab clearfix">
                        <?php while(have_rows('service')): the_row(); ?>
                        <li>
                            <a href="#">
                                <div class="service-tab-img">
                                    <div class="img-responsive"><?php echo fx_get_image_tag( get_sub_field( 'background_image' ), false, true ); ?> </div>
                                </div>
                                <div class="service-tab-text"><?php echo get_sub_field( 'title' ); ?></div>
                            </a>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                    <p><?php echo get_field( 'paragraph_detail' ); ?></p>
                    <?php if( $button = get_field( 'button' )  ): ?>
                        <a href="<?php echo esc_url( $button['url'] ); ?>" <?php echo !empty($button['target']) ? 'target="' . esc_url( $button['target'] ) . '"': ''; ?> class="btn btn-tertiary">
                            <?php echo $button['title']; ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </div>