    <link href="../wp-content/themes/championscaffold/assets/css/components/FxAccordion.css" rel="stylesheet">
    <!-- Common styling for accordion sections -->
  <section class="fxa-accordion fx-accordion js-accordion">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="fx-acc-title">
                        <h3>Lorem ipsum dolor</h3> <i class="icon-plus"></i>
                        <p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                    </div>
                    <div class="fxa-accordion__panels">
                        <div class="fxa-accordion__panel js-accordion-item" data-accordion-id="0">
                            <div class="fxa-accordion__panel__toggle js-accordion-headline" type="button" data-accordion-id="0">At vero eos et accusam et justo duo dolores et ea rebum</div>
                            <div class="fxa-accordion__panel__content">
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                                <p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>       
                        <div class="fxa-accordion__panel js-accordion-item" data-accordion-id="1">
                            <div class="fxa-accordion__panel__toggle js-accordion-headline" type="button" data-accordion-id="1">Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna 
                                aliquyam erat, sed diam voluptua. </div>
                            <div class="fxa-accordion__panel__content">
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                                <p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>      
                        <div class="fxa-accordion__panel js-accordion-item" data-accordion-id="2">
                            <div class="fxa-accordion__panel__toggle js-accordion-headline" type="button" data-accordion-id="2">At vero eos et accusam et justo duo dolores et ea rebum</div>
                            <div class="fxa-accordion__panel__content">
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                                <p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>                                                      
                    </div>
                </div>
            </div>
        </div>
 </section>
 <script src="../wp-content/themes/championscaffold/assets/js/components/FxAccordion.js"></script>
<!-- JS to initialize accordion sections -->