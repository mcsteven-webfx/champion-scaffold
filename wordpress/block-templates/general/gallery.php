<?php
// Get the gallery ID
$gallery_id = get_field('gallery_id');
?>
<section class="case-study-gallery">
        <div class="container-fluid">
            <?php echo do_shortcode('[fx_photo_gallery gallery_id=' . $gallery_id . ']'); ?>
    </div>
</section>