<?php
	$background	= get_field( 'background' );
?>
<section class="wysiwyg <?php if( $background ) { echo $background; } ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="wysiwyg__content">
					<?php echo get_field( 'content' ); ?>
				</div>
			</div>
		</div>
	</div>
	
</section>
