<?php
	$position	= get_field( 'position' );
?>
<section class="half-and-half">
    <div class="half-and-half-image <?php if( $position ) { echo $position; } ?>">
       <?php echo fx_get_image_tag( get_field( 'image' ), false, true ); ?> 
    </div>
    <div class="half-and-half-text <?php if( $position ) { echo $position; } ?> ">
        <h5><?php echo get_field( 'text' ); ?></h5>
        <h2><?php echo get_field( 'heading' ); ?></h2>
        <p><?php echo get_field( 'paragraph' ); ?></p>
        <?php if( $button = get_field( 'button' )  ): ?>
            <a href="<?php echo esc_url( $button['url'] ); ?>" <?php echo !empty($button['target']) ? 'target="' . esc_url( $button['target'] ) . '"': ''; ?> class="btn btn-secondary">
                <?php echo $button['title']; ?>
            </a>
        <?php endif; ?>
    </div>
</section>