  
  <section class="fxa-accordion fx-accordion js-accordion">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="fx-acc-title">
                        <h3><?php echo get_field( 'title' ); ?></h3> 
                        <p><?php echo get_field( 'paragraph' ); ?></p>
                    </div>
                    <?php if(have_rows('accordion') ): ?>
                        <div class="fxa-accordion__panels">
                        <?php $i = 0; ?>
                        <?php while(have_rows('accordion')): the_row(); ?>
                            <div class="fxa-accordion__panel js-accordion-item" data-accordion-id="<?php echo esc_attr( $i ) ;?>">
                                <div class="fxa-accordion__panel__toggle js-accordion-headline">
                                    <?php echo get_sub_field( 'title' ); ?>
                                </div>
                                <div class="fxa-accordion__panel__content">
                                    <p><?php echo get_sub_field( 'paragraph' ); ?></p>
                                </div>
                            </div> 
                        <?php ++$i; ?>
                        <?php endwhile; ?>
                                                      
                        </div>
                <?php endif; ?>
                    
                </div>
            </div>
        </div>
 </section>
