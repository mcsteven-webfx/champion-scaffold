<div class="container-fluid no-padding-container">
        <section class="full-width-image-text-box-overlay">
            <div class="full-width-image-text-img">
                <?php echo fx_get_image_tag( get_field( 'image' ), false, true ); ?> 
                <div class="full-width-image-text-box-pattern">
                    <img src="../wp-content/themes/championscaffold/assets/img/full-img-pattern.png" alt="">
                </div>
            </div>
            
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="full-width-image-text-content">
                            <h3><?php echo get_field( 'title' ); ?> </h3>
                            <p><?php echo get_field( 'paragraph' ); ?></p>
                            <?php if( $button = get_field( 'button' )  ): ?>
                                <a href="<?php echo esc_url( $button['url'] ); ?>" <?php echo !empty($button['target']) ? 'target="' . esc_url( $button['target'] ) . '"': ''; ?> class="btn btn-primary">
                                    <?php echo $button['title']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>