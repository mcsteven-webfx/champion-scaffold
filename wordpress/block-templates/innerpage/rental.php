
 <section class="rental-cart-section js-load-more-block">
        <div class="container">
            <div class="rental-title clearfix">
                  <?php if( get_field('file') ): ?>
                    <a href=" <?php the_field('file'); ?>" class="btn btn-tertiary" >
                    Download PDF of All Products <svg xmlns="http://www.w3.org/2000/svg" width="11.813" height="15.75" viewBox="0 0 11.813 15.75">
                    <path id="file-download" d="M6.891,4.184V0H.738A.737.737,0,0,0,0,.738V15.012a.737.737,0,0,0,.738.738H11.074a.737.737,0,0,0,.738-.738V4.922H7.629A.74.74,0,0,1,6.891,4.184Zm2.352,6.5L6.276,13.629a.525.525,0,0,1-.74,0L2.571,10.685a.492.492,0,0,1,.346-.842H4.922V7.383a.492.492,0,0,1,.492-.492H6.4a.492.492,0,0,1,.492.492V9.844H8.9A.492.492,0,0,1,9.242,10.685ZM11.6,3.23,8.586.215A.738.738,0,0,0,8.063,0H7.875V3.938h3.938V3.75A.736.736,0,0,0,11.6,3.23Z" fill="#a32c1c"/>
                  </svg></a>
                  <?php endif; ?>
                <h3>Rental Equipment</h3>
            </div>
            <div class="rental-search-content clearfix">
            <form action="/" method="get" class="search-content">
                 <div class="rental-search">
                    <!--<input type="text" placeholder="Search Equipment">-->
                    <input type="Search" name="s" id="s" value="" data-swplive="true" placeholder="Search Equipment">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </form>
               
                <div class="rental-search-btn">
                	
                    <a href="<?php the_permalink(537);?>" class="btn btn-secondary">Add Items to Quote <svg xmlns="http://www.w3.org/2000/svg" width="26.514" height="23.568" viewBox="0 0 26.514 23.568">
                        <path id="shopping-cart" d="M24.31,13.87,26.486,4.3a1.1,1.1,0,0,0-1.077-1.35H7.329L6.907.883A1.1,1.1,0,0,0,5.824,0H1.1A1.1,1.1,0,0,0,0,1.1v.737a1.1,1.1,0,0,0,1.1,1.1H4.322L7.555,18.755a2.578,2.578,0,1,0,3.086.394h9.65a2.577,2.577,0,1,0,2.928-.479l.254-1.117A1.1,1.1,0,0,0,22.4,16.2H10.04l-.3-1.473H23.233A1.1,1.1,0,0,0,24.31,13.87Z" fill="#adadad"/>
                      </svg>
                    <input class="additems" type="text" id="inc" value="0"></input>
                      </a>
                </div>
            </div>
            <!-- add gantt chart -->
            <div class="rental-content" id="rental-content">
                <?php
                 $no = 0;
                 while(have_rows('rental_cart_page')): the_row();
                 	$no++;
                 	$price = get_sub_field( 'price' ) > 0 ? get_sub_field( 'price' ) : 0;
                  ?>
                    <div class="rental-item">
                        <div class="rental-img">
                            <?php echo fx_get_image_tag( get_sub_field( 'image' ), false, true ); ?> 
                        </div>
                        <?php echo fx_get_image_tag( '209','rental-img' );?>
                        <div class="rental-text">
                            <div class="rental-details">
                                <h2><?php echo get_sub_field( 'title' ); ?></h2>
                                <?php echo get_sub_field( 'paragraph' ); ?>
                            </div>
                            <div class="rental-qty clearfix">
                                <div class="rental-time">$<span id="price-<?= $no ?>" value="<?php echo $price ; ?>"><?php echo $price ; ?></span>/Week</div>
                                <div class="rental-bottom">
                                    <div class="rental-count"> 
                                        <input type="text" placeholder="0" oninput="document.getElementById('price-<?= $no ?>').innerHTML = parseInt(this.value === '' ? 1 : this.value) * parseInt(document.getElementById('price-<?= $no ?>').getAttribute('value'))">
                                    </div>
                                    <div class="rental-item-bttn">
                                        <button class="btn btn-primary add-rental" onclick="buttonClick();">
                                            <div class="icon-check"></div>Add to Cart
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="loading-area">
                <p class="load-more__counter js-load-more-counter"></p>
                <div class="loading-bar">
                    <div class="loading-bar-active"></div>
                </div>
                <a href="#" class="btn btn-tertiary load-more js-load-more">Load More</a>
            </div>
           
        </div>
    </section>
<script>
    var i = 0;
    function buttonClick() {
        i++;
        document.getElementById('inc').value = i;
    }
</script>