 <section class="rental-quote-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-push-6">
                    <div class="rental-sidebar">
                        <div class="rental-sidebar-top clearfix">
                            <h4>Equipment to Quote</h4>
                            <div class="rental-quote-bttn">
                                <a href="<?php the_permalink(448);?>" class="btn btn-tertiary hidden-md-down">Add More Equipment</a>
                                <a href="#" class="btn btn-primary hidden-lg">Get Quote</a>
                                <a href="#" class="btn btn-secondary hidden-lg">Add More Equipment</a>
                            </div>
                        </div>
                        
                        <div class="rental-sidebar-content">
                        <?php while(have_rows('rental_cart_page')): the_row(); ?>
                                <div class="rental-sidebar-item">
                                    <div class="rental-sidebar-img">
                                         <?php echo fx_get_image_tag( get_sub_field( 'image' ), false, true ); ?> 
                                    </div>
                                    <div class="rental-sidebar-text">
                                        <h2><?php echo get_sub_field( 'title' ); ?></h2>
                                        <div class="rental-qty clearfix">
                                            <div class="rental-time"><?php echo $price ; ?></div>
                                            <div class="rental-count">
                                                <p>Qty:</p>
                                                <input type="text" placeholder="1">
                                            </div>
                                            <div class="equipment-close">
                                                <i class="icon-close"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php endwhile; // end of the loop. ?>
                        </div>
                    
                    
                        
                        
                    </div>
                </div>
                <div class="col-lg-6 col-lg-pull-6">
                    <div class="rental-quote-text">
                         <p>Please note: this is only a request for rentals, not a guarantee. Inventory levels are subject to change, a inventory manager will reach out to you to confirm your request.</p>
                        <div class="rental-quote-form">
                            
                            <?php echo do_shortcode('[contact-form-7 id="519" title="Rental Quote" html_id="cf7-form-519"]'); ?>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>