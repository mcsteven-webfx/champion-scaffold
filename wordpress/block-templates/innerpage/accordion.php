<section class="faq">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h3 class="faq__subtitle subtitle">
					<?php the_field( 'subtitle' ); ?>
				</h3>
				<h2 class="faq__title">
					<?php the_field( 'headline' ); ?>
				</h2>
				<?php if( have_rows( 'faqs' ) ): ?>
					<div class="faq__items js-accordion">
						<?php $i = 0; ?>
						<?php while( have_rows( 'faqs' ) ): the_row(); ?>
						
							<article class="faq__item js-accordion-item" data-accordion-id="<?php echo esc_attr( $i ) ;?>"><?php // JS to add is-expanded if block is expanded ?>
								
								<div class="faq__item__heading js-accordion-headline">
									<h4 class="faq__item__headline h5">
										<?php the_sub_field( 'question' ); ?>
									</h4>
									<button class="faq__item__btn js-accordion-button">
										<i class="faq__item__toggle icon-plus icon--expand"></i><?php // JS to add icon--collapse if block is expanded ?>
									</button>
								</div>
								<div class="faq__item__answer">
									<?php the_sub_field( 'answer' ); ?>
								</div>
							</article>
							<?php ++$i; ?>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>