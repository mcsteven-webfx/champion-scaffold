<?php
	$title 		= apply_filters( 'the_title', get_the_title() );
	$excerpt 	= apply_filters( 'the_excerpt', get_the_excerpt() );
?>
<a class="search-rental" href="<?php the_permalink(); ?>">
	<div class="search-result__img-container">
		<?php echo fx_get_image_tag( get_post_thumbnail_id(), 'search-rental__img', 'thumbnail' , 'image' ); ?>
	</div>
	<h2 class="search-rental__title"><?php echo $title; ?></h2>
	<div class="search-rental__excerpt">
		<?php echo $excerpt; ?>
	</div>
</a>