<section class="contact-section">
	<div class="container-fluid">
		<div class="contact-top">
			<div class="contact-content">
				<div class="row">
					<div class="col-lg-12">
						<div class="text-center">
							<?php the_field( 'intro_text' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section class="contact-form">
			<div class="contact-content">
				<?php the_field( 'cf7_shortcode' ); ?>
			</div>
		</section>
	</div>
</section>
