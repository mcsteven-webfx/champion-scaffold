<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"> 
    <!-- font-family: 'Lexend', sans-serif; -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet"> 
    <!-- font-family: 'Roboto Condensed', sans-serif; -->
    <?php wp_head(); ?>
</head>
<body id="top"  <?php body_class(); ?>>
     <?php do_action( 'fxtheme_before_header' ); ?>
    <?php
        // gets client logo image set in Theme Settings
        $logo_id    = fx_get_client_logo_image_id(); 
        $home_url   = get_home_url();
    ?>
    <header id="page-header" class="page-header">
        <div class="top-header clearfix">
            <div class="top-header-content">
                <div class="top-bttn-bar hidden-xs-down">
                    <a href="<?php the_permalink(460);?>" class="btn btn-primary">Get a Quote</a>
                </div>
                <div class="top-phone-bar hidden-xs-down">
                    <a href="tel:(510) 778-9932"><i class="icon-phone"></i> Phone (510) 778-9932</a>
                </div>
                <div class="top-call-bar">
                    <a href="tel:(510) 778-9932"><i class="icon-call-24hr"></i> 24 Hour <span>Call-Out Number</span> (510) 778-9932</a>
                </div>
            </div>
        </div>
        
        <div class="middle-header">
            <div class="container-fluid clearfix">
                <div class="logo">
                    <a class="site-logo" alt="Champion Scaffold" href="<?php echo esc_url( $home_url ); ?>">
                        <?php echo fx_get_image_tag( $logo_id, 'logo' ); ?>
                    </a>
            
                </div>
                <div class="toggle-menu hidden-xs-down hidden-lg"><i class="icon-menu"></i> MENU</div>
                    <div class="top-search">
                        <form action="/" method="get" class="search-content">
                            <div class="search-content">
                                <div class="js-search-toggle">
                                    <i class="icon-search"></i> Search
                                </div>
                                <div class="js-search-active">
                                    <input type="Search" name="s" id="s" value="" data-swplive="true" placeholder="Search...">
                                    <button type="submit"><i class="icon-search"></i></button>
                                </div>
                            </div>
                        </form>              
                    </div>
                
                  <nav class="nav-primary li.menu-item-has-children ">
                    <?php
                        wp_nav_menu( array (
                            'theme_location'    => 'main_menu',
                            'container'         => 'nav-primary',
                            'menu_class'        => 'clearfix'
                        ) );
                    ?>
                </nav>
            </div>
        </div>
        <div class="nav-fixed hidden-sm-up clearfix">
            <div class="fixed-quote-btn"><a href="<?php the_permalink(460);?>" class="btn btn-primary">Get a Quote</a></div>
            <div class="toggle-menu"><i class="icon-menu"></i> MENU</div>
            <div class="call-menu">
                <a href="tel:(510) 778-9932"><i class="icon-phone"></i>Call</a>
            </div>
        </div>
    </header>
