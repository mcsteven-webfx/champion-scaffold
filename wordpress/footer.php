<footer class="page-footer">
        <div class="top-footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-lg-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="footer-info">
                                    <div class="footer-logo">
                                        <?php
		                                    $home_url = get_home_url();
		                                ?>
		                                <a href="<?php echo esc_url( $home_url ); ?>">
		                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php echo get_bloginfo('name');?>">
		                                </a>
                                    </div>
                                    <p><?php the_field('license','options') ?></p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="footer-address">
                                    <h4>Contact</h4>
                                    <p><?php the_field('address','options') ?><br>Phone: <?php the_field('phone','options') ?>   |   Fax: <?php the_field('fax','options') ?><br><a href="mailto:<?php the_field('email','options') ?>"><?php the_field('email','options') ?></a></p>
                                    <ul class="social-media">
                                        <li>
                                            <a href="<?php the_field('facebook','options') ?>" target="_blank"><i class="icon-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="<?php the_field('instagram','options') ?>" target="_blank"><i class="icon-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a href="<?php the_field('linkedin','options') ?>" target="_blank"><i class="icon-linkedin"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="footer-link">
                            <h4>Quick Links</h4>
                            <?php
                                wp_nav_menu( array(
                                    'menu' => 'Footer Quick Links'
                                    ,'container'       => 'ul'
                                    ,'container_class' => ''
                                ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer">
            <div class="container clearfix">
                <ul>
                    <li><a href="<?php the_permalink(760);?>">Site Credits</a></li>
                    <li><a href="<?php the_permalink(708);?>">Sitemap</a></li>
                    <li><a href="<?php the_permalink(3);?>">Privacy Policy</a></li>
                    <li>Copyright &copy; <?php echo date('Y'); ?>. All Rights Reserved.</li>
                </ul>
                <?php get_template_part('partials/back-to-top'); ?>
            </div>
        </div>
    </footer>
    
     <?php wp_footer(); ?>
    </body>
</html>