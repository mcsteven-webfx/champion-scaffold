<?php get_header(); ?>
<?php get_template_part('partials/masthead'); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <section class="single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="post-thumbnail">
                        <?php the_post_thumbnail('full'); ?>
                    </div>
                    <div class="post-content">
                        <?php the_content(); ?>
                    </div>
                    <?php get_template_part( 'partials/social-share' ); ?>
                </div>
                <div class="col-lg-4">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; endif; ?>
<?php get_footer(); ?>